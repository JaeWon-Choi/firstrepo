﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TimeCheck : MonoBehaviour {
	public DateTime currentTime;
	void Start() {
		currentTime = DateTime.Now;
	}
	void Update() {
		if (Input.GetKeyDown (KeyCode.Space)) {
			Debug.Log ((DateTime.Now - currentTime).ToString ());
		}
	}
}